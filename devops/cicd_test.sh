set -e

activate_venv() {
  if [ -f ".venv/bin/activate" ]; then
    source .venv/bin/activate # Linux / Mac OS
  else
    source .venv/Scripts/activate # Windows
  fi
}

ROOT=$(pwd)

mkdir _test_root
pushd _test_root

python -m venv .venv
activate_venv

pip install cookiecutter
cookiecutter $ROOT addon_module=test_addon --no-input

deactivate

pushd test_addon

bash extra/bootstrap.sh
git describe --tags

activate_venv # venv in test_addon folder
python -m pytest

popd

popd

rm -rf _test_root