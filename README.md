# Cookiecutter for a Blender Addon

## Description

This is a [cookiecutter](https://cookiecutter.readthedocs.io/) to create a new Blender addon project matching several conventions.

## Features

Python packages:

- fake-bpy-module-2.82 for Blender API autocompletion
- black for code formatting
- flake8 for linting
  - flake8-black for linting of formatting issues
  - flake8-bugbear for static code analysis bug detection
  - pep8-naming for PEP8 naming conventions (use `# noqa` for Blender symbols that do not match this convention)
- auto-changelog for generation of CHANGELOG.md based on conventional commits
- pytest for unit testing

A configuration for VSCode is also provided.

Python and CI/CD tooling:
- A gitlab CI/CD script with jobs to run flake8 on the code and to generate a zip archive as artifact that can be installed in Blender.
  - Running of unit tests is left up to you because they can require a complex Blender setup
- A script to inject version number in the project (`pyproject.toml` and `bl_info`) based on git tags.
- A script to tag a commit for release: it inject the corresponding version number, generate the changelog and apply the tag.

## How to use

### Introduction

You need to install cookiecutter, an executable python package to create folder hierarchies from templates. I recommand using [pipx](https://pipxproject.github.io/pipx/) to avoid polluting your global python installation, but it is up to you.

Run cookiecutter on this repository:

```bash
cookiecutter https://gitlab.com/c2ba_cookiecutters/blender_addon_cookiecutter
```

It will ask you to fill some fields, the most important one being `addon_module` that will serve as the root folder and as the module folder for your addon.

### Automatic setup

Once created, enter the folder and just run the following line from a shell compatible terminal:

```bash
cd PROJECT_ROOT_FOLDER
bash ./extra/bootstrap.sh
```

It should destroy itself at the end. For more explanation on what this script is doing, read the the next section.

### Manual setup

If you prefer you can run the following commands instead of running the bootstrap script.

You need [poetry](https://python-poetry.org/) to create the developer virtual environment.

If you don't have poetry installed globally, first create the virtual env and install poetry inside. Skip this step if you already have poetry.

```bash
python -m venv .venv
# Here activate your virtual env - depends on your terminal and OS
#   - For git bash on Windows it would be: source .venv/Scripts/activate
python -m pip install poetry
```

Then run:

```bash
poetry install
```

Initialize a git repository and make a first commit:

```bash
cd PROJECT_ROOT_FOLDER
git init
git add .
git commit -m "feat: initial commit"
```

If you have a remote URL for your project, add it and optionally push your changes:

```bash
git remote add origin URL
git push # optional, can be done later
```

At this point you can create a first version tag and generate an initial changelog

```bash
git tag v0.1.0
# With the virtual env enabled
auto-changelog --tag-prefix v
git add CHANGELOG.md
git commit -m "doc: add changelog"
```

## Auto Changelog usage

With the virtual env activated:

```bash
auto-changelog -u --tag-prefix v
```

It will generate `CHANGELOG.md` with unrealeased changes and based on tags prefixed with "v" letter (e.g. v0.1.0).

Try to follow the convention https://www.conventionalcommits.org/en/v1.0.0/ for your commits but don't worry, `auto-changelog` will still work if some of your commits do not match the convention.

## Injecting version number

Inject the version number into the project:
```bash
python -m extra.version_scripts.inject_version
```

This version number will take into account the previous version tag, the current commit and the state of your local repository (dirty or not).

To create a brand new version, just commit or dicard/stash your changes and then run:

```bash
python -m extra.version_scripts.tag_version_for_release <major> <minor> <bugfix> <message>
```