import logging

bl_info = {
    "name": "{{cookiecutter.addon_name}}",
    "description": "{{cookiecutter.addon_description}}",
    "author": "{{cookiecutter.author_name}}",
    "version": (0, 1, 0),
    "blender": (2, 83, 0),
    "location": "View3D",
    "warning": "This addon is still in development.",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Unknown",
}

__version__ = ".".join(str(i) for i in bl_info["version"])

if __name__ == "__main__":
    logger = logging.getLogger()
else:
    logger = logging.getLogger(__name__)


def register():
    logging.info(f"Register {__name__} version {__version__}")
    pass


def unregister():
    logging.info(f"Unregister {__name__} version {__version__}")
    pass


if __name__ == "__main__":
    register()
