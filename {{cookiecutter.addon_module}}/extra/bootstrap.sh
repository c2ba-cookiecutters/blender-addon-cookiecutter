set -e

if ! [ -x "$(command -v python)" ]; then
  echo "python not found is PATH"
  exit 1
fi

CURRENT_DIR=`dirname "$0"`
pushd $CURRENT_DIR/..

activate_venv() {
  if [ -f ".venv/bin/activate" ]; then
    source .venv/bin/activate # Linux / Mac OS
  else
    source .venv/Scripts/activate # Windows
  fi
}

if ! [ -x "$(command -v poetry)" ]; then
  python -m venv .venv
  activate_venv
  python -m pip install poetry
fi

poetry install

activate_venv

git init
git add .
git commit -m "feat: initial commit"

REMOTE_URL="{{cookiecutter.repository_url}}"
if ! [ -z "$REMOTE_URL" ]; then
  git remote add origin $REMOTE_URL
fi

touch CHANGELOG.md
git add CHANGELOG.md
git commit -m "doc: add CHANGELOG.md"

python -m extra.version_scripts.tag_version_for_release 0 1 0 "Version 0.1.0"

rm extra/bootstrap.sh

popd