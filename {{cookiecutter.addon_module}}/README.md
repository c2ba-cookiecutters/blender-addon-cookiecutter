# {{cookiecutter.addon_name}}

## Introduction

{{cookiecutter.addon_description}}

## Developer setup

After cloning the repository, you can setup a developer virtual environment with [poetry](https://python-poetry.org/):

```bash
poetry install
```

If you don't have poetry installed globally, you can create a virtual env, install poetry inside and run the command:

```bash
python -m venv .venv
# Here activate your virtual env - depends on your terminal and OS
python -m pip install poetry
poetry install
```

The developer environment contains useful packages for code formatting, linting, `bpy` module completion, etc.